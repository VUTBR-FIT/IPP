

#MKA:xcerny63
'''
Created on Mar 28, 2016

@author: xcerny63
'''
import sys
import os.path
import fileinput
from array import *
from sys import stdin
from fileinput import filename

DEBUGGING = False

def fileExists(filename):
    try :
        file = open(filename, 'r')
    except IOError :
        return False
    else :
        file.close()
        return True 

def printError(message):
    print("ERROR: ", message, sep=' ', end='\n', file=sys.stderr)
    
def printHelp():
    print(
          "--help\t\t\tnapoveda\n"
          + "--input=filename\tzadaný vstupní textový soubor filename v UTF-8 s popisem dobře specifikovaného konečného automatu\n"
          + "--output=filename\ttextový výstupní soubor filename (opět v UTF-8) s popisem výsledného ekvivalentního konečného automatu v předepsaném formátu výstupu.\n"
          + "-f,\n--find-non-finishing\thledá neukončující stav zadaného dobře specifikovaného konečného automatu (automat se nevypisuje). Nalezne-li jej, bez odřádkování jej vypíše na výstup; jinak vypíše pouze číslici 0. (Před hledáním se provede validace na dobrou specifikovanost automatu.) Parametr nelze kombinovat s parametrem -m (resp. --minimize).\n"
          + "-m,\n--minimize\t\tprovede minimalizaci dobře specifikovaného konečného automatu (viz algoritmus IFJ, přednáška 11, snímek 23/35). Parametr nelze kombinovat s parametrem -f (resp. --find-non-finishing).\n"
          + "-i,\n--case-insensitive\tnebude brán ohled na velikost znaků při porovnávání symbolů či stavů (tj. a = A, ahoj = AhOj nebo A b = a B); ve výstupu potom budou všechna velká písmena převedena na malá")

def printMKA(arrState, arrInput, arrRule, strStart, arrEndState):
    result = "(\n{"
    
    arrState.sort()
    for i in range(len(arrState)) :
        if i != 0 :
            result += ", "
        result += str(arrState[i])
    result += "},\n{"
    
    arrInput.sort()
    for i in range(len(arrInput)) :
        if i != 0 :
            result += "', "
        result += "'" + str(arrInput[i])
    result += "'},\n{\n"   
    
    keys = []
    for key, value in enumerate(arrRule) :
        keys.append(value)
    keys.sort()
    for i in range(len(keys)) :
        arrRule[keys[i]] = sorted(arrRule[keys[i]], key=lambda x : x[0])
        for j in range(len(arrRule[keys[i]])) :
            if i != 0 or j != 0:
                result += ",\n"
            result += str(keys[i]) + " '" + str(arrRule[keys[i]][j][0]) + "' -> " + str(arrRule[keys[i]][j][1])
        
    result += "\n},\n" + str(strStart) + ",\n{" 
    
    arrEndState.sort()
    for i in range(len(arrEndState)) :
        if i != 0 :
            result += ", "
        result += str(arrEndState[i])
    result += "}\n)"
    return (result)

def validateInput(intArgc, arrArgv):
    paramInput = False
    inputName = ""
    paramOutput = False
    outputName = ""
    paramF = False
    paramM = False
    paramI = False
    error = 0
    
    if DEBUGGING : print("Validace-vstupu:")
    for i in range(1, intArgc) :
        if ("-f" == arrArgv[i]) or ("--find-non-finishing" == arrArgv[i]) :
            if paramF == False :
                paramF = True
                continue
            else :
                error = 1
                break
            
        if ("-m" == arrArgv[i]) or ("--minimize" == arrArgv[i]) :
            if paramM == False :
                paramM = True
                continue
            else :
                error = 1
                break
            
        if ("-i" == arrArgv[i]) or ("--case-insensitive" == arrArgv[i]) :
            if paramI == False :
                paramI = True
                continue
            else :
                error = 1
                break
            
        if (arrArgv[i].startswith("--input=")) :
            parts = arrArgv[i].split("=")
            if paramInput == False and len(parts) == 2 :
                inputName = parts[1]
                paramInput = True
                continue
            else :
                error = 1
                break
            
        if (arrArgv[i].startswith("--output=")) :
            parts = arrArgv[i].split("=")
            if paramOutput == False and len(parts) == 2 :
                outputName = parts[1]
                paramOutput = True
                continue
            else :
                error = 1
                break
            
        error = 1
        break    
    if (paramF == True) and (paramM == True) :
        error = 1
        
    if paramInput :
        # testuji zda se jedna o soubor
        if not os.path.isfile(inputName) :
            error = 2
        if not fileExists(inputName) :
            error = 2
        if not os.access(inputName, os.R_OK) :
            error = 2
    if paramOutput :
        try :
            file = open(outputName, 'w')
        except IOError :
            error = 3
        else :
            file.close()
    if DEBUGGING :
        print("\tError:\t%s" % error)
        print("\tParamInput:\t%s" % paramInput)
        print("\tInputName:\t%s" % inputName)
        print("\tParamOutput:\t%s" % paramOutput)
        print("\tOutputName:\t%s" % outputName)
        print("\tMinimite:\t%s" % paramM)
        print("\tFindNFinished:\t%s" % paramF)
        print("\tCaseSensitiv:\t%s" % paramI)
                
    return (error, paramInput, inputName, paramOutput, outputName, paramM, paramF, paramI)

def getFileContents(filename = ""):
    if DEBUGGING : print("Ziskani contents ze STDIN / FILE")
    contents = ""
    error = 0
    if len(filename) == 0 :
        if DEBUGGING : print("\tCteni ze STDIN")
        for line in sys.stdin.readlines() :
            contents += str(line)        
    else :
        try :
            if DEBUGGING : print("\tCteni ze souboru")
            file = open(filename, 'r')
            contents = file.read()
        except IOError :
            if DEBUGGING : print("\tPri cteni ze souboru nastala chyba")
            error = 2
        else :
            file.close()
    if error == 0 and len(contents) == 0 : error = 60
    if DEBUGGING : print("\tUkonceni nacitani dat")
    return (error, contents)

def saveOutput(filename, contents):
    if DEBUGGING : print("Ukladani contents na STDOUT / FILE")
    error = 0
    if len(filename) == 0 :
        sys.stdout.write(contents)
    else :
        try :
            if DEBUGGING : print("\tUkladani do souboru")
            file = open(filename, 'w')
            file.write(contents)
        except IOError :
            if DEBUGGING : print("\tPri zapisu do souboru doslo k chybe")
            error = 3
        else :
            file.close()      
    return (error)

def findTraps(arrInput, arrRules, arrEndState, countTraps = False):
    if DEBUGGING : print("Hledani pasti")
    searched = []
    count = 0
    founded = False
    for i in arrRules :
        if i in arrEndState : continue
        founded = False
        if len(arrInput) == len(arrRules[i]) :
            for j in range(len(arrRules[i])) :
                if arrRules[i][j][1] != i :
                    founded = False
                    break
                elif j == len(arrRules[i])-1 : founded = True
        if founded : 
            if countTraps : count += 1
            else : break  
    if not founded : i = ""  
    if countTraps : i = count
    return (i) 

def minimize(arrState, arrInput, arrRule, strStart, arrEndState):
    restart = True
    arrNuclear = [arrEndState, list(set(arrState) - set(arrEndState))]
    #print(arrNuclear)
    #print(arrState, joinSubArray(arrNuclear))
    while restart :
        restart = False
        for alpha in arrInput :
            nuclear = True
            temp = -1
            start = 0
            arrayJoined = joinSubArray(arrNuclear)
            while (nuclear) :
                nuclear = False
                for i in range(start, len(arrState)) :
                    state = arrayJoined[i]
                    #print('->', state, alpha)
                    #print(temp, arrRule[state][inSubArray(arrRule[state], alpha)][1], searchInSubArray(arrNuclear, arrRule[state][inSubArray(arrRule[state], alpha)][1]))
                        
                    if temp == -1 or searchInSubArray(arrNuclear, state) != searchInSubArray(arrNuclear, arrState[i-1]) : temp = searchInSubArray(arrNuclear, arrRule[state][inSubArray(arrRule[state], alpha)][1])
                    if  searchInSubArray(arrNuclear, state) != searchInSubArray(arrNuclear, arrState[i-1]) : start = i
                    if temp != searchInSubArray(arrNuclear, arrRule[state][inSubArray(arrRule[state], alpha)][1]) :
                        #print(arrNuclear)
                        #print(state, temp, searchInSubArray(arrNuclear, arrRule[state][inSubArray(arrRule[state], alpha)][1]), arrRule[state][inSubArray(arrRule[state], alpha)][1])
                        nuclear = True
                        restart = True
                        break
                if nuclear :
                    #start = i+1
                    if len(arrNuclear) < searchInSubArray(arrNuclear, state) + 2 :
                        arrNuclear.append([state])
                    else : arrNuclear[searchInSubArray(arrNuclear, state) + 1].append(state)
                    arrNuclear[searchInSubArray(arrNuclear, state)].remove(state)
                    #print(arrNuclear)
    for i in range(len(arrNuclear)) :
        arrNuclear[i].sort()
        new = '_'.join(arrNuclear[i])
        if len(arrNuclear[i]) < 2 : continue
        arrState.append(new)
        
        arrRule[new] = arrRule[arrNuclear[i][0]]
        
        for x in arrState :
            for y in range(len(arrRule[x])) :
                if arrRule[x][y][1] in arrNuclear[i] :
                    arrRule[x][y][1] = new
        for j in range(len(arrNuclear[i])) :
            if arrNuclear[i][j] in arrState : arrState.remove(arrNuclear[i][j])
            arrRule.pop(arrNuclear[i][j])   
            
    return (arrState, arrInput, arrRule, strStart, arrEndState)

def validateKA(contents):
    result = ""
    comment = False
    commentOn = True
    state = 0
    error = False    
    retError = 0
    value = ""
    statesKA = []
    inputKA = []
    rulesKA = {}
    startKA = ""
    endStatesKA = []
    value1 = ""
    value2 = ""
    if len(contents) == 0 : error = True
    for i in range(0, len(contents)) :
        if error : break
        
        c = contents[i]
        
        if commentOn == True :
            if c == '#' : 
                comment = True
                continue
            if comment == True :
                if c == '\n' or c == '\r' :
                    comment = False
                continue
           
        commentOn = True
        
        if DEBUGGING : 
            print("->" + c)
            print("State:%s" % state)
        
        if state == 0 :
            if c == '(' :
                state += 1
                continue
        elif state == 1 : # Mnozina stavu
            if c == '{' :
                state += 1
                continue
        elif state == 2 :
            if c == '}' or c == ',':
                if len(value) > 0 :
                    if value not in statesKA :
                        if value.startswith('_') or str.isdigit(value[0]) or value.endswith('_') :
                            error = True
                        statesKA.append(value)
                    value = ""
                    if c == '}' :
                        state += 1
                    continue
                if c == '}' and len(statesKA) == 0 :
                    error = True
                    retError = 61
            elif str.isalnum(c) or c == '_' :
                value += c
                commentOn = False
                continue
            elif str.isspace(c) : 
                if len(value) > 0 :
                    if value not in statesKA :
                        if value.startswith('_') or str.isdigit(value[0]) or value.endswith('_') :
                            error = True
                        statesKA.append(value)
                    value = ""
                    state = 200
                continue
        elif state == 200 :
            if c == '}' or c == ',' :
                if c == '}' : state = 3
                else : state = 2
                continue
        elif state == 3 :
            if c == ',' :
                state += 1
                continue  
        elif state == 4 : # Mnozina vstupni abecedy
            if c == '{' :
                state += 1
                continue  
        elif state == 5 :
            if c == '}' or c == ',' :
                if len(value) > 0 :
                    if value not in inputKA :
                        inputKA.append(value)
                    value = ""
                    if c == '}' : state += 1
                    continue
                else : error = True
                if len(inputKA) == 0 : retError = 61 
            elif c == "'" and len(value) == 0 :
                state = 500
                commentOn = False
                continue
        elif state == 500 :
            if len(value) == 0 :
                value += c
                commentOn = False
                continue
            elif len(value) == 1 :
                if value != "'" and c == "'" :
                    state = 5
                    commentOn = False
                    continue
                elif value == "'" and c == "'" :
                    value += c
                    commentOn = False
                    continue
                else : error = True
            elif len(value) == 2 :
                if c == "'" :
                    state = 5
                    commentOn = False
                    continue
                else : error = True
            else : error = True
        elif state == 6 :
            if c == ',' :
                state += 1
                continue 
        elif state == 7 : # Mnozina pravidel prechodu
            if c == '{' :
                state += 1
                continue  
        elif state == 8 :
            if c == '}' :
                state += 1
                continue
            elif str.isalnum(c) or c == '_' :
                commentOn = False
                value += c
                state = 800
                continue
        elif state == 800 :
            if str.isalnum(c) or c == '_' :
                commentOn = False
                value += c
                continue
            elif str.isspace(c) or c == "'" :
                if c == "'" :
                    state += 2
                    commentOn = False
                else : state += 1
                continue
            elif c == '-' :
                error = True
                retError = 62
            else : error = True
        elif state == 801 :
            if c == "'" :
                commentOn = False
                state += 1
                continue
        elif state == 802 :
            if len(value1) == 0 :
                value1 += c
                commentOn = False
                continue
            elif len(value1) == 1 :
                if value1 != "'" and c == "'" :
                    state += 1
                    commentOn = False
                    continue
                elif value1 == "'" and c == "'" :
                    value1 += c
                    commentOn = False
                    continue
                elif value1 == "'" :
                    error = True
                    retError = 62
                else : error = True
            elif len(value1) == 2 :
                if c == "'" :
                    state += 1
                    commentOn = False
                    continue
                else : error = True
            else : error = True
        elif state == 803 :
            if c == '-' :
                state += 1
                commentOn = False
                continue
        elif state == 804 :
            if c == '>' :
                state += 1
                continue
            else : error = True
        elif state == 805 :
            if str.isalnum(c) or c == '_' :
                commentOn = False
                value2 += c
                continue
            elif len(value2) > 0 and (str.isspace(c) or c == ',' or c == '}' ):
                if (value not in statesKA) or (value1 not in inputKA) or (value2 not in statesKA) :
                    error = True
                    retError = 61
                if value in rulesKA :
                    x = inSubArray(rulesKA[value], value1)
                    if x == -1 :
                        rulesKA[value].append([value1, value2])
                    else :
                        if rulesKA[value][x][1] != value2 :
                            error = True
                            retError = 62
                else :
                    rulesKA[value] = [[value1, value2]]
                value = ""
                value1 = ""
                value2 = ""   
                if c == '}' : state = 9
                elif c == ',' : state += 1
                else : state += 2
                continue
        elif state == 806 :
            if str.isalnum(c) or c == '_' :
                value += c
                state = 800
                continue
        elif state == 807 :
            if c == ',' :
                state -= 1
                continue
            elif c == '}' :
                state = 9
                continue
        elif state == 9 :
            if c == ',' :
                state += 1
                continue   
        elif state == 10 :
            if str.isalnum(c) or c == '_' :
                startKA += c
                commentOn = False
                continue
            elif (c == ',' or str.isalnum(c)) and len(startKA) > 0 :
                if startKA not in statesKA :
                    error = True
                    retError = 61
                if c == ',' : state += 2
                else : state += 1
                continue
            elif c == ',' :
                error = True
                retError = 60
        elif state == 11 :
            if c == ',' :
                state += 1
                continue  
        elif state == 12 : # Mnozina koncovych stavu
            if c == '{' :
                state += 1
                continue  
        elif state == 13 :
            if c == '}' or c == ',':
                if len(value) > 0 :
                    if value not in endStatesKA :
                        if value.startswith('_') or str.isdigit(value[0]) or value.endswith('_') :
                            error = True
                        endStatesKA.append(value)
                        if value not in statesKA :
                            error = True
                            retError = 61
                    value = ""
                    if c == '}' :
                        state += 1
                    continue
                if c == '}' and len(endStatesKA) == 0 :
                    error = True
                    retError = 62
            elif str.isalnum(c) or c == '_' :
                value += c
                commentOn = False
                continue
            elif str.isspace(c) : 
                if len(value) > 0 :
                    if value not in endStatesKA :
                        if value.startswith('_') or str.isdigit(value[0]) or value.endswith('_') :
                            error = True
                        endStatesKA.append(value)
                        if value not in statesKA :
                            error = True
                            retError = 61
                    value = ""
                    state = 1300
                continue
        elif state == 1300 :
            if c == '}' or c == ',' :
                if c == '}' : state = 3
                else : state = 2
                continue
        elif state == 3 :
            if c == ',' :
                state += 1
                continue  
        elif state == 14 :
            if c == ')' :
                state += 1
                continue  
        if c == ' ' or c == '\n' or c == '\r' or c == '\t' : continue
        error = True  
    if DEBUGGING :
        if error : print("Nastala chyba pri parsovani vstupu")
        else : print("Nenastala chyba pri parsovani vstupu")  
        print(statesKA)
        print(inputKA)   
        print(rulesKA)
        print(startKA)  
        print(endStatesKA)    
    if error :
        if retError == 0 :  retError = 60
    else :
        if retError == 0 :
            # Odstraneni DUPLICIT
            openList = [startKA]
            closeList = []
            while len(openList) > 0 :
                for x in range(len(rulesKA[openList[0]])) :
                    if (rulesKA[openList[0]][x][1] not in openList) and (rulesKA[openList[0]][x][1] not in closeList) : openList.append(rulesKA[openList[0]][x][1])
                closeList.append(openList[0])
                openList.remove(openList[0])
            if len(closeList) != len(statesKA) : retError = 62
            if retError == 0 :
                if inArray(statesKA, startKA) < 0 :
                    retError = 61
            if retError == 0 :
                for i in range(len(endStatesKA)):
                    if inArray(statesKA, endStatesKA[i]) < 0 :
                        retError = 61
                    break
            if findTraps(inputKA, rulesKA, endStatesKA, True) > 1 :
                error = True
                retError = 62
    if DEBUGGING : print("Error:%s" % retError)
    return (retError, statesKA, inputKA, rulesKA, startKA, endStatesKA)

def inArray(array ,searched):
    exist = -1
    if searched in array :
        for key, value in enumerate(array) :
            if searched == value :
                exist = key
                break    
    return (exist)

def inSubArray(array, searched, index = 0, startIndex = -1):
    exist = -1
    for key, value in enumerate(array) :
        if searched == value[index] and startIndex < key:
            exist = key
            break    
    return (exist)

def searchInSubArray(array, searched) :
    exist = -1
    for key, value in enumerate(array) :
        for index in range(len(value)) :
            if searched == value[index]:
                exist = key
                break  
        if exist != -1 : break  
    return (exist)

def isInputAlphabet(char):
    input = "(){}\'->,. #"
    if str.isalnum(char) or char == ' ' or input.find(char) >= 0 or char == '\n' or char == '\r' or char == '\t' :
        return (True)
    else :
        return (False)
def joinSubArray(array):
    result = []
    for i in range(len(array)) :
        array[i].sort()
        for j in range(len(array[i])) :
            result.append(array[i][j])
    return (result)
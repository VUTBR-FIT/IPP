<?php

#JSN:xcerny63

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
require_once ROOT . 'Configuration/configuration.php';

use Vendor\Classes\InputParams;

function __autoload($class)
{
    require_once(ROOT . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php');
}

$objInput = new InputParams((int) $argc, $argv);
$objInput->run();

exit($objInput->getError());

<?php

#JSN:xcerny63

namespace Vendor\Library\XMLCreator;

use Vendor\Library\Debugger\Debugger as Debugger;

/**
 * Description of XmlElementCreator1
 *
 * @author Lukas
 */
class XMLElementCreator extends XMLCreator implements IXMLElementCreator
{

    /**
     * Jméno elementu
     *
     * @var string
     */
    private $key;

    /**
     * Pole atributů
     *
     * @var array
     */
    private $vars;

    /**
     * Regularni výraz pro validaci prvního znaku ve jménu elementu
     *
     * @var string
     */
    public static $nameStartChar = "[\_:A-Za-z\x{C0}-\x{D6}\x{D8}-\x{F6}\x{F8}-\x{2FF}\x{370}-\x{37D}\x{37F}-\x{1FFF}\x{200C}-\x{200D}\x{2070}-\x{218F}\x{2C00}-\x{2FEF}\x{3001}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFFD}\x{10000}-\x{EFFFF}]";

    /**
     * Regularní výraz pro validaci ostatních znaků ve jméně elemntu
     *
     * @var string
     */
    public static $nameChar = "[\_:A-Za-z\x{C0}-\x{D6}\x{D8}-\x{F6}\x{F8}-\x{2FF}\x{370}-\x{37D}\x{37F}-\x{1FFF}\x{200C}-\x{200D}\x{2070}-\x{218F}\x{2C00}-\x{2FEF}\x{3001}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFFD}\x{10000}-\x{EFFFF}.\-0-9\x{B7}\x{0300}-\x{036F}\x{203F}-\x{2040}\-]*";

    public function __construct($objConfig, $key, $value = '', $vars = array())
    {
        parent::__construct($objConfig);
        $this->key = $this->validKey($key, $this->objConfiguration);
        $this->vars = $vars;
        $this->setValue($value);
    }

    public function getStringRepresentation()
    {
        if ($this->intError != \Error::ALL_OK) {
            return (FALSE);
        }
        if ($this->objConfiguration->getOnLongRepresentation() || (strlen($this->getValue()) > 0)) {
            $result = "<$this->key" . $this->getVars() . ">";
            $value = $this->getValue();
            if ($value === FALSE) return (FALSE);
            $result .= $value;
            $result .= "</$this->key>";
        } else {
            $result = "<$this->key" . $this->getVars() . "/>";
        }
        return ($result);
    }

    public static function isValidKey($key)
    {
        $elementName = "/^" . self::$nameStartChar . self::$nameChar . "$/u";
        return ((preg_match($elementName, $key) == 1) && (strtolower(substr($key, 0, 3)) != 'xml')) ? TRUE : FALSE;
    }

    private function validKey($key)
    {
        if ($this->isValidKey($key)) {
            return ($key);
        } else {
            $key2 = $this->replaceInKey($key);
            if ($this->isValidKey($key2) === FALSE) {
                $this->intError = \Error::BAD_ELEMENT_NAME;
                //Debugger::info("xxx {a}", array('a' => $key));
                //Debugger::info("aaa {a}", array('a' => $key2));
                Debugger::error("Nevalidni hodnota {var}", array('var' => $this->objConfiguration->getSubst()));
                return ("");
            } else {
                return ($key2);
            }
        }
    }

    private function replaceInKey($key)
    {
        $result = "";
        if (strtolower(substr($key, 0, 3)) != 'xml') {
            $first = substr($key, 0, 1);
            if (preg_match("/^" . self::$nameStartChar . "$/u", $first) == 1) {
                $result = $first;
            } else {
                $result = $this->objConfiguration->getSubst();
            }
            $other = substr($key, 1);
        } else {
            $result = $this->objConfiguration->getSubst();
            $other = substr($key, 3);
        }

        for ($i = 0; $i < strlen($other); $i++) {
            if (preg_match("/^" . self::$nameChar . "$/u", substr($other, $i, 1)) == 1) {
                $result .= substr($other, $i, 1);
            } else {
                $result .= $this->objConfiguration->getSubst();
            }
        }
        return ($result);
    }

    public function setValue($value)
    {
        if ($this->intError != \Error::ALL_OK) {
            return;
        }

        if (is_array($value)) {
            $this->data = $value;
        } else {
            if (is_object($value)) {
                $this->data[] = $value;
            } elseif (is_string($value)) {
                if (strlen($value) != 0) {
                    if ($this->objConfiguration->getOnTransfString() === FALSE) {
                        $this->vars['value'] = $value;
                    } else {
                        $this->data[] = $value;
                    }
                }
            } elseif (is_numeric($value)) {
                if ($this->objConfiguration->getOnTransfNumber() === FALSE) {
                    $this->vars['value'] = $value;
                } else {
                    $this->data[] = $value;
                }
            } elseif (($value === null) || ($value === FALSE) || ($value === TRUE)) {
                if ($this->objConfiguration->getOnLiterar()) {
                    if ($value === NULL) {
                        $this->addChildElement($this, strval('null'));
                    } elseif ($value === FALSE) {
                        $this->addChildElement($this, strval('false'));
                    } elseif ($value === TRUE) {
                        $this->addChildElement($this, strval('true'));
                    }
                } else {
                    if ($value === NULL) {
                        $this->vars['value'] = strval('null');
                    } elseif ($value === FALSE) {
                        $this->vars['value'] = strval('false');
                    } elseif ($value === TRUE) {
                        $this->vars['value'] = strval('true');
                    }
                }
            }
        }
    }

    private function getValue()
    {
        if ($this->intError != \Error::ALL_OK) {
            return (FALSE);
        }
        $result = "";
        foreach ($this->data as $value) {
            if ($this->intError != \Error::ALL_OK) {
                return (FALSE);
            }
            if (is_string($value) || is_numeric($value)) {
                $result = $this->escapeString($value);
                if (count($this->data) != 1) {
                    $this->intError = -1;
                    Debugger::error("Chyba v algoritmu XmlElementCreator::getValue()");
                    break;
                }
            } elseif (is_object($value) && ($value instanceof XmlElementCreator)) {
                $str = $value->getStringRepresentation();
                if ($str === FALSE) {
                    $this->intError = $value->getError();
                    return (FALSE);
                } else {
                    $result .= $str;
                }
            } else {
                $this->intError = -1;
                Debugger::error("Chyba v algoritmu XmlElementCreator::getValue()");
                break;
            }
        }
        return ($result);
    }

    private function getVars()
    {
        $result = "";
        foreach ($this->vars as $key => $value) {
            $result .= " $key=\"" . $this->escapeString($value) . "\"";
        }
        return ($result);
    }

    private function escapeSpecilChar($value)
    {
        $result = $value;
        if ($this->objConfiguration->getOnSpecial()) {
            $specialChars = array('&', '<', '>', "'", '"');
            $validChars = array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;');
            $result = str_replace($specialChars, $validChars, $value);
        }
        return($result);
    }

    private function escapeString($value)
    {
        $result = $this->escapeSpecilChar($value);
        return ($result);
    }

}

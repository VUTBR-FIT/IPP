<?php

#JSN:xcerny63

if (!defined('ROOT')) {
    define('ROOT', dirname(dirname(dirname(__DIR__))) . '/');
}

use Vendor\Library\XMLCreator\XMLElementCreator;

function __autoload($class)
{
    require_once ROOT . str_replace('\\', '/', $class) . '.php';
}

$config = new Vendor\Library\XMLCreator\Configuration();
$config->setOnSpecial(TRUE);
$xml = new \Vendor\Library\XMLCreator\XMLCreator($config);
$xml->addElement('root', '', array('value' => 'prvniRoot'));

$elemRoot = new XMLElementCreator($config, 'root', array(
    new XMLElementCreator($config, 'a', 'aa'),
    new XMLElementCreator($config, 'b', 'bb'),
        ), array('keyRoot' => 'hodnotaRooot', 'keyRootu' => 'hodnotaroor'));
$xml->addElement($elemRoot);
$xml->addElement(new XMLElementCreator($config, 'testsRoot', array(
    new XMLElementCreator($config, 'sss', 'testovani eskapovani')
)));

var_dump($xml->getContext());

<?php

#JSN:xcerny63

namespace Vendor\Library\XMLCreator;

/**
 * Description of XmlCreator
 *
 * @author Lukas
 */
class XMLCreator implements IXMLCreator
{

    /**
     * Kodování výstupního XML souboru
     *
     * @var string
     */
    private $encoding = 'UTF-8';

    /**
     * Verze výstupního XML souboru
     *
     * @var string
     */
    private $version = '1.0';

    /**
     * Proměnná pro zapnutí vypisování XML hlavičky
     * TRUE - bude se vypisovat
     * FALSE - Nebude se vypisovat
     *
     * @var bool
     */
    private $onHeader = TRUE;

    /**
     * Objekt obsahující nastaveni XML souboru
     *
     * @var Configuration
     */
    protected $objConfiguration = NULL;

    /**
     * Všechno je v pořádku - 0
     * Špatně tvořená třída - 1
     * Nevalidni klič elementu - 2
     *
     * @var int
     */
    protected $intError = \Error::ALL_OK;

    /**
     * Obsahuje hodnoty
     *
     * Struktura elementu
     * object(
     *      'key' => string,
     *      'value' => mixed,
     *      'vars' => array()
     * )
     * @var array
     */
    protected $data = array();

    public function __construct($objConfig)
    {
        if ($objConfig === NULL) {
            Debugger::critical('XMLCreator::__construct() First parameter missed or null given');
            $this->intError = -2;
        }
        $this->objConfiguration = $objConfig;
    }

    public final function setHeader($encoding = 'UTF-8', $version = '1.0')
    {
        $this->encoding = $encoding;
        $this->version = $version;
    }

    private function getHeader()
    {
        $result = "";
        if ($this->objConfiguration->getOnXMLHeader()) {
            $result = "<?xml version=\"$this->version\" encoding=\"$this->encoding\"?>";
        }
        return ($result);
    }

    public function addElement($element, $value = '', $vars = array())
    {
        if ($this->intError != \Error::ALL_OK) {
            return;
        }
        if (is_object($element) && ($element instanceof XMLElementCreator)) {
            $this->intError = $element->getError();
            $this->data[] = $element;
        } elseif (is_array($element)) {
            foreach ($element as $value) {
                if ($this->intError != \Error::ALL_OK) return;
                $this->addElement($value);
            }
        } elseif (is_string($element)) {
            $this->addElement($this->createElement($element, $value, $vars));
        }
    }

    public function createElement($strKey, $value = '', $vars = array())
    {
        if ($this->intError != \Error::ALL_OK) {
            return (NULL);
        }
        return (new XMLElementCreator($this->objConfiguration, $strKey, $value, $vars));
    }

    public function addChildElement(XmlElementCreator $objElem, $element, $value = '', $vars = array())
    {
        if (is_object($element) && ($element instanceof XmlElementCreator)) {
            return ($objElem->addElement($element));
        } else {
            $new = $this->createElement($element, $value, $vars);
            return ($objElem->addChildElement($objElem, $new));
        }
    }

    public final function __toString()
    {
        return ($this->getContext());
    }

    public function getContext()
    {
        if ($this->intError != \Error::ALL_OK) {
            return (FALSE);
        }
        $strData = "";
        foreach ($this->data as $value) {
            if ($this->intError != \Error::ALL_OK) break;
            $strData .= $value->getStringRepresentation();
            $this->intError = $value->getError();
        }
        if ($this->intError != \Error::ALL_OK) {
            return (FALSE);
        }
        return ($this->getHeader() . $strData);
    }

    public function getError()
    {
        return ($this->intError);
    }

    public function setOffHeader()
    {
        $this->onHeader = FALSE;
    }

    public function setOnHeader()
    {
        $this->onHeader = TRUE;
    }

}

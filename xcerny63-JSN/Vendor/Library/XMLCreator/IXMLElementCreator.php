<?php

#JSN:xcerny63

namespace Vendor\Library\XMLCreator;

/**
 *
 * @author Lukas
 */
interface IXMLElementCreator
{

    public function getStringRepresentation();

    public static function isValidKey($key);

    public function setValue($value);
}

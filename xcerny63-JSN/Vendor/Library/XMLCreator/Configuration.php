<?php

#JSN:xcerny63

namespace Vendor\Library\XMLCreator;

/**
 * Description of Configuration
 *
 * @author Lukas
 */
class Configuration
{

    /** @var bool prepinac -n */
    private $onXMLHeader = TRUE;

    /** @var bool prepinac -r */
    private $onRootElement = FALSE;
    private $rootName = '';

    /** @var string prepinac -h */
    private $subst = '-';

    /** @var string prepinac --array-name */
    private $arrayName = 'array';

    /** @var string prepinac --item-name */
    private $itemName = 'item';

    /** @var bool prepinac -s */
    private $onTransfString = FALSE;

    /** @var bool prepinac -i */
    private $onTransfNumber = FALSE;

    /** @var bool prepinac -l */
    private $onLiterar = FALSE;

    /** @var bool prepinac -c
     * Eskapování speciálních znaků
     */
    private $onSpecial = FALSE;

    /** @var bool prepinac -a, --array-size */
    private $onArraySize = FALSE;

    /** @var bool prepinac -t, --index-items */
    private $onIndexItem = FALSE;

    /** @var int prepinac --start */
    private $startIndexItem = 1;

    /**
     * TRUE -> <key></key>
     * FALSE -> <key/>
     *
     * @var bool
     */
    private $onLongRepresentation = FALSE;

    public function getOnXMLHeader()
    {
        return $this->onXMLHeader;
    }

    public function getOnRootElement()
    {
        return $this->onRootElement;
    }

    public function getRootName()
    {
        return $this->rootName;
    }

    public function getSubst()
    {
        return $this->subst;
    }

    public function getArrayName()
    {
        return $this->arrayName;
    }

    public function getItemName()
    {
        return $this->itemName;
    }

    public function getOnTransfString()
    {
        return $this->onTransfString;
    }

    public function getOnTransfNumber()
    {
        return $this->onTransfNumber;
    }

    public function getOnLiterar()
    {
        return $this->onLiterar;
    }

    public function getOnSpecial()
    {
        return $this->onSpecial;
    }

    public function getOnArraySize()
    {
        return $this->onArraySize;
    }

    public function getOnIndexItem()
    {
        return $this->onIndexItem;
    }

    public function getStartIndexItem()
    {
        return $this->startIndexItem;
    }

    public function getOnLongRepresentation()
    {
        return $this->onLongRepresentation;
    }

    public function setOnXMLHeader($onXMLHeader)
    {
        $this->onXMLHeader = (bool) $onXMLHeader;
    }

    public function setOnRootElement($onRootElement)
    {
        $this->onRootElement = (bool) $onRootElement;
    }

    public function setRootName($rootName)
    {
        $this->rootName = $rootName;
    }

    public function setSubst($subst)
    {
        $this->subst = $subst;
    }

    public function setArrayName($arrayName)
    {
        $this->arrayName = $arrayName;
    }

    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    public function setOnTransfString($onTransfString)
    {
        $this->onTransfString = (bool) $onTransfString;
    }

    public function setOnTransfNumber($onTransfNumber)
    {
        $this->onTransfNumber = (bool) $onTransfNumber;
    }

    public function setOnLiterar($onLiterar)
    {
        $this->onLiterar = (bool) $onLiterar;
    }

    public function setOnSpecial($onSpecial)
    {
        $this->onSpecial = (bool) $onSpecial;
    }

    public function setOnArraySize($onArraySize)
    {
        $this->onArraySize = (bool) $onArraySize;
    }

    public function setOnIndexItem($onIndexItem)
    {
        $this->onIndexItem = (bool) $onIndexItem;
    }

    public function setStartIndexItem($startIndexItem)
    {
        $this->startIndexItem = (int) $startIndexItem;
    }

    public function setOnLongRepresentation($onLongRepresentation)
    {
        $this->onLongRepresentation = (bool) $onLongRepresentation;
    }

}

<?php

#JSN:xcerny63

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vendor\Library\XMLCreator;

/**
 *
 * @author Lukas
 */
interface IXMLCreator
{

    /**
     * Nastaví verzi XML souboru a jeho kódování
     *
     * @param string $encoding
     * @param string $version
     */
    public function setHeader($encoding = 'UTF-8', $version = '1.0');

    /**
     * Tato funkce přídává objekty do aktulního elementu
     * V proměnné $element - může být array, object, nebo string
     * Pokud je v proměnné $element array, tak celé pole se prochází a přidává
     * jeho prvky do aktuálního elementu.
     * Pokud je v proměnné $element object, tak je tento objekt přidán
     * do aktuálního elementu.
     * Pokud je v proměnné $element string, tak je vytvořen nový obejkt, kterému
     * jsou předány proměnné $element, $value a $vars. Následně je vložen
     * do aktuálníhho elementu.
     *
     *
     * @param mixed $element
     * @param string $value
     * @param array $vars
     */
    public function addElement($element, $value = '', $vars = array());

    /**
     * Vytvoří nový objekt a vrací ukazatel na něj.
     *
     * @param string $strKey
     * @param string $value
     * @param array $vars
     *
     * @return XmlElementCreator Vytvořený element
     */
    public function createElement($strKey, $value = '', $vars = array());

    /**
     * Vnoří nový či již vytvořený element do elementu v proměné $objElem.
     * V proměnné $element - může být array, object, nebo string
     * Pokud je v proměnné $element array, tak celé pole se prochází a přidává
     * jeho prvky do elementu v $objElem.
     * Pokud je v proměnné $element object, tak je tento objekt přidán
     * do elementu v $objElem.
     * Pokud je v proměnné $element string, tak je vytvořen nový obejkt, kterému
     * jsou předány proměnné $element, $value a $vars. Následně je vložen
     * do elementu v $objElem.
     *
     * @param \Vendor\Library\XMLCreator\XmlElementCreator $objElem
     * @param mixed $element
     * @param string $value
     * @param array $vars
     */
    public function addChildElement(XmlElementCreator $objElem, $element, $value = '', $vars = array());

    /**
     * Převede element a jeho vnořené elementy do textové podoby.
     *
     * @return mixed Při úspěchu vrací string, při neuspěchu vrací FALSE
     */
    public function getContext();

    /**
     * @return int Vrací číslo chyby
     */
    public function getError();

    /**
     * Vypne XML hlavičku, nebude generovaná.
     * Defaultně je zapnuta.
     */
    public function setOffHeader();

    /**
     * Zapne CML hlavič, bude genrováná.
     * Defaltné je zapnuta.
     */
    public function setOnHeader();
}

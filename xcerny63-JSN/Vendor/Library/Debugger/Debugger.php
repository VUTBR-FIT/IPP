<?php

#JSN:xcerny63

namespace Vendor\Library\Debugger;

class Debugger implements IDebugger
{

    const FILE_NAME = 'errors.log';

    public static function alert($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::ALERT, $message, $context);
    }

    public static function critical($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::CRITICAL, $message, $context);
    }

    public static function debug($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::DEBUG, $message, $context);
    }

    public static function emergency($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::EMERGENCY, $message, $context);
    }

    public static function error($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::ERROR, $message, $context);
    }

    public static function info($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::INFO, $message, $context);
    }

    public static function notice($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::NOTICE, $message, $context);
    }

    public static function warrning($message, array $context = array())
    {
        Debugger::appendLine(DebuggerLevel::WARNING, $message, $context);
    }

    public static function log($level, $message, array $context = array())
    {
        return null;
    }

    private static function appendLine($level, $message, $context)
    {
        $message = $level . ': ' . Debugger::interpolate($message, $context) . PHP_EOL;
        #file_put_contents(self::FILE_NAME, $message, FILE_APPEND);
        self::stdErr($message);
    }

    private static function stdErr($message)
    {
        fwrite(STDERR, $message);
    }

    private static function interpolate($message, $context = array())
    {
        $replace = array();
        foreach ($context as $key => $value) {
            $replace['{' . $key . '}'] = $value;
        }
        return strtr($message, $replace);
    }

}

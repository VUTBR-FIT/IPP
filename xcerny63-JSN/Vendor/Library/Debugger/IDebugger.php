<?php

#JSN:xcerny63

namespace Vendor\Library\Debugger;

interface IDebugger
{

    /**
     * Systém je nepoužitelný
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function emergency($message, array $context = array());

    /**
     * Je nutné ihned provést akci.
     *
     * Například: Stránka je mimo provoz, nedostupná. Metoda by měla spustit
     * SMS upozornění.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function alert($message, array $context = array());

    /**
     * Kritické podmínky.
     *
     * Komponenta aplikace je nedostupná, neočekávaná vyjímka.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function critical($message, array $context = array());

    /**
     * Běhové chyby, nevyžadují okamžitou akci.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function error($message, array $context = array());

    /**
     * Vyjimkové události, ale nejsou chybami.
     *
     * Příklad: Nevhodné použití API.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function warrning($message, array $context = array());

    /**
     * Normální, ale podstatné události.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function notice($message, array $context = array());

    /**
     * Zajímavé události
     *
     * Příklad: SQL logy, přihlášení.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function info($message, array $context = array());

    /**
     * Detailní ladící informace.
     *
     * @param type $message
     * @param array $context
     * @return null
     */
    public static function debug($message, array $context = array());

    /**
     * Logs with an arbitrary level.
     *
     * $param int $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function log($level, $message, array $context = array());
}

<?php

#JSN:xcerny63

namespace Vendor\Classes;

use Vendor\Library\Debugger\Debugger,
    \Vendor\Library\XMLCreator\Configuration,
    Vendor\Library\XMLCreator\XMLElementCreator;

/**
 * Description of inputParams
 *
 * @author Lukas
 */
class InputParams
{

    /** @var string SourcePath */
    private $sourcePath = '';

    /** @var string SavePath */
    private $savePath = '';

    /** @var int */
    private $countParam;

    /** @var array */
    private $allParams;

    /** @var int */
    private $intError = \Error::ALL_OK;

    /** @var \Vendor\Library\XMLCreator\Configuration */
    private $xmlElemConfig;

    /** @var JsonToXML */
    private $JSON2XML;
    private $boolHelp = FALSE;

    public function __construct($param, array $allParam)
    {
        $this->countParam = (int) $param;
        $this->allParams = $allParam;
    }

    public function run()
    {
        $this->xmlElemConfig = new Configuration();
        $this->parseParams();
        if ($this->boolHelp || ($this->intError != \Error::ALL_OK)) return;

        if ((strlen($this->sourcePath) != 0) && ((file_exists($this->sourcePath) !== TRUE) || !is_readable($this->sourcePath) || (($content = @file_get_contents($this->sourcePath)) === FALSE))) {
            $this->intError = \Error::INPUTFILE_NOT_EXIST;
            Debugger::error("Vstupní soubor neexistuje nebo neni citelny");
        } elseif ((strlen($this->sourcePath) == 0)) {
            $content = "";
            $counter = 10;
            while (($c = fgetc(STDIN)) !== FALSE) {
                $content .= $c;
                if (!$counter && (strlen($content) == 0)) break;
                $counter--;
            }
            if (strlen($content) == 0) {
                Debugger::error("Nebyl zadan vstupni ci vystuni soubor");
                $this->intError = \Error::BAD_PARAMS;
                return (FALSE);
            }
        }

        if ($this->intError != \Error::ALL_OK) {
            return;
        } elseif ((file_exists($this->savePath) === TRUE) && (!is_writable($this->savePath))) {
            $this->intError = \Error::OUTPUTFILE_ERR;
            Debugger::error("Vystupni soubor existuje a neni prepsatelny");
        } else {
            $this->JSON2XML = new JsonToXML($this->xmlElemConfig);
            $this->intError = $this->JSON2XML->generateXML($content, $this->xmlElemConfig);

            if ($this->intError == \Error::ALL_OK) {
                $this->saveResult();
            }
        }
    }

    private function parseParams()
    {
        if ($this->intError != \Error::ALL_OK) {
            return (FALSE);
        }
        $inputParams = array();
        for ($i = 1; $i < $this->countParam; $i++) {
            $param = explode('=', $this->allParams[$i]);
            if (in_array($param[0], $inputParams)) {
                $this->intError = \Error::BAD_PARAMS;
                Debugger::error("Neplatne zadani parametru (zadala jste vicekrát pramaetr {param})", array('param' => $param[0]));
            }
            switch ($param[0]) {
                case \Params::HELP:
                    if ($this->countParam == 2) {
                        echo (\Params::getHelp());
                    } else {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->boolHelp = TRUE;
                    break;
                case \Params::INPUT:
                    if ((count($param) != 2) || (strlen($param[1]) == 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } else {
                        $this->sourcePath = $param[1];
                    }
                    break;
                case \Params::OUTPUT:
                    if ((count($param) != 2) || (strlen($param[1]) == 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } else {
                        $this->savePath = $param[1];
                    }
                    break;
                case \Params::H:
                    if ((count($param) != 2) || (strlen($param[1]) == 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } else {
                        $this->xmlElemConfig->setSubst($param[1]);
                    }
                    break;
                case \Params::N:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnXMLHeader(FALSE);
                    break;
                case \Params::R:
                    if ((count($param) != 2) || (strlen($param[1]) == 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } elseif (XMLElementCreator::isValidKey($param[1]) === FALSE) {
                        $this->intError = \Error::BAD_INPUT_DATA;
                    } else {
                        $this->xmlElemConfig->setRootName($param[1]);
                        $this->xmlElemConfig->setOnRootElement(TRUE);
                    }
                    break;
                case \Params::ARRAY_NAME:
                    if ((count($param) != 2) || (strlen($param[1]) == 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } elseif (XMLElementCreator::isValidKey($param[1]) === FALSE) {
                        $this->intError = \Error::BAD_INPUT_DATA;
                    } else {
                        $this->xmlElemConfig->setArrayName($param[1]);
                    }
                    break;
                case \Params::ITEM_NAME:
                    if ((count($param) != 2) || (strlen($param[1]) == 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } elseif (XMLElementCreator::isValidKey($param[1]) === FALSE) {
                        $this->intError = \Error::BAD_INPUT_DATA;
                    } else {
                        $this->xmlElemConfig->setItemName($param[1]);
                    }
                    break;
                case \Params::S:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnTransfString(TRUE);
                    break;
                case \Params::I:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnTransfNumber(TRUE);
                    break;
                case \Params::L:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnLiterar(TRUE);
                    break;
                case \Params::C:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnSpecial(TRUE);
                    break;
                case \Params::A:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnArraySize(TRUE);
                    break;
                case \Params::ARRAY_SIZE:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnArraySize(TRUE);
                    break;
                case \Params::T:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnIndexItem(TRUE);
                    break;
                case \Params::INDEX_ITEMS:
                    if (count($param) != 1) {
                        $this->intError = \Error::BAD_PARAMS;
                    }
                    $this->xmlElemConfig->setOnIndexItem(TRUE);
                    break;
                case \Params::START:
                    if ((count($param) != 2) || (strlen($param[1]) == 0) || (is_numeric($param[1]) === FALSE) || ((int) $param[1] < 0)) {
                        $this->intError = \Error::BAD_PARAMS;
                    } else {
                        $this->xmlElemConfig->setStartIndexItem($param[1]);
                    }
                    break;
                default:
                    $this->intError = \Error::BAD_PARAMS;
                    break;
            }
            if ($this->intError != \Error::ALL_OK) {
                Debugger::error("Chyba u paramateru {param}", array('param' => $this->allParams[$i]));
                return (FALSE);
            } else {
                $inputParams[] = $param[0];
            }
        }

        $inputParams = array_flip($inputParams);

        if ((array_key_exists(\Params::INDEX_ITEMS, $inputParams) && array_key_exists(\Params::T, $inputParams))
                || (array_key_exists(\Params::A, $inputParams) && array_key_exists(\Params::ARRAY_SIZE, $inputParams))
                || ((array_key_exists(\Params::START, $inputParams) === TRUE )
                && (array_key_exists(\Params::INDEX_ITEMS, $inputParams) === FALSE)
                && (array_key_exists(\Params::T, $inputParams) === FALSE))) {
            Debugger::error("Neplatna kombinace parametru");
            $this->intError = \Error::BAD_PARAMS;
            return (FALSE);
        }
        return (TRUE);
    }

    /**
     *
     * @return int
     */
    public function getError()
    {
        return ($this->intError);
    }

    public function saveResult()
    {
        $content = $this->JSON2XML->getXmlContent();
        if ($content !== FALSE) {
            if (strlen($this->savePath) != 0) {
                file_put_contents($this->savePath, $this->JSON2XML->getXmlContent());
            } else {
                echo $this->JSON2XML->getXmlContent();
            }
        } else {
            $this->intError = $this->JSON2XML->getError();
        }
    }

}

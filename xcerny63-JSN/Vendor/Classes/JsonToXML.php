<?php

#JSN:xcerny63

namespace Vendor\Classes;

use Vendor\Library\XMLCreator\XMLCreator,
    Vendor\Library\XMLCreator\XMLElementCreator,
    Vendor\Library\XMLCreator\Configuration,
    Vendor\Library\Debugger\Debugger;

/**
 * Description of JsonToXML
 *
 * @author Lukas
 */
class JsonToXML
{

    /** @var bool prepinac -n */
    private $onXMLHeader = TRUE;

    /** @var XMLCreator */
    private $objXml = NULL;

    /** @var Configuration */
    private $objXmlConfig = NULL;
    private $intError = \Error::ALL_OK;

    public function __construct($objXMLElemConfig)
    {
        if ($objXMLElemConfig === NULL) {
            Debugger::critical('Json2XML - config = NULL');
            $this->intError = \Error::CREATING_CLASS_FAILED;
        } else {
            $this->objXmlConfig = $objXMLElemConfig;
            $this->objXml = new XMLCreator($objXMLElemConfig);
        }
    }

    public function generateXML($json)
    {
        if ($this->intError != \Error::ALL_OK) {
            return ($this->intError);
        }
        if ($this->objXmlConfig->getOnXMLHeader() === FALSE) {
            $this->objXml->setOffHeader();
        }
        if (($input = json_decode($json)) === NULL) {
            Debugger::error("Vstupní soubor obsahuje nevalidni obsah");
            return (\Error::INPUT_BAD_STRUCTURE);
        } elseif ($this->objXmlConfig->getOnRootElement()) {
            if (XMLElementCreator::isValidKey($this->objXmlConfig->getRootName())) {
                $this->objXml->addElement($this->objXmlConfig->getRootName(), $this->generateValue($input));
            } else {
                $this->intError = \Error::BAD_ROOT_NAME;
            }
        } else {
            $this->objXml->addElement($this->generateValue($input));
        }
        if ($this->objXml->getError() != \Error::ALL_OK) {
            $this->intError = \Error::BAD_ELEMENT_NAME;
        }
        return ($this->intError);
    }

    public function generateValue($value)
    {
        if ($this->intError != \Error::ALL_OK) {
            return;
        }
        if (is_array($value)) {
            return ($this->arrayToXML($value));
        } elseif (is_object($value)) {
            return ($this->objectToXML($value));
        } elseif (is_string($value)) {
            return ((string) $value);
        } elseif (is_numeric($value)) {
            return ((int) floor($value));
        } else {
            return ($value);
        }
    }

    public function arrayToXML($value)
    {
        if ((XMLElementCreator::isValidKey($this->objXmlConfig->getArrayName()) === FALSE) || (XMLElementCreator::isValidKey($this->objXmlConfig->getItemName()) === FALSE)) {
            Debugger::error("Spatne jmeno pro pole nebo pro polozku");
            $this->intError = \Error::BAD_INPUT_DATA;
            return ($this->intError);
        }
        $arrData = array();
        $index = $this->objXmlConfig->getStartIndexItem();
        foreach ($value as $data) {
            $arrData[] = $this->objXml->createElement($this->objXmlConfig->getItemName(), $this->generateValue($data), ($this->objXmlConfig->getOnIndexItem()) ? array('index' => $index) : array());
            $index++;
        }
        $array = $this->objXml->createElement($this->objXmlConfig->getArrayName(), $arrData, (($this->objXmlConfig->getOnArraySize() ? array('size' => count($value)) : array())));

        return ($array);
    }

    private function objectToXML($value)
    {
        $arrData = array();
        foreach ($value as $key => $data) {
            $arrData[] = $this->objXml->createElement($key, $this->generateValue($data));
        }
        return $arrData;
    }

    public function getXmlContent()
    {
        if ($this->intError != \Error::ALL_OK) {
            return (FALSE);
        }
        $result = $this->objXml->getContext();
        if ($result === FALSE) {
            $this->intError = \Error::BAD_ELEMENT_NAME;
        }
        return ($result);
    }

    public function getError()
    {
        return ($this->intError);
    }

}

<?php

/*
 * 0 => JSON
 * 1 => errCode
 * 2 => Params
 * 3 => inputParam OFF
 * 4 => outputParam OFF
 * 5 => IN & OUT OFF
 */
$commands = array(
    array('01', 0, "-h=_"),
    array('12', 0, "-h=aaa"),
    array('12', 0, "-c -h=xx"),
    array('12', 0, "-c -h=_ -r=_roor"),
    array('14', 0, "-h=X"),
    array('test01', 0, ""),
    array('test02', 0, "-n -r=\"koren\""),
    array('test03', 0, "-n", FALSE, TRUE),
    array('test04', 0, "-n -r=\"koren\"", TRUE),
    array('test05', 0, "-n -r=\"koren\" --array-name=\"pole\"", TRUE, TRUE),
    /* 10 */ array('test06', 0, "-l"),
    array('test07', 0, "-r=root --item-name=pól -a"),
    array('test08', 0, "-n -s --start=0 -t"),
    array('test09', 0, "-n -s"),
    array('test10', 0, "-r=\"tešt-élěm\""),
    array('test11', 0, "-c -l -r=rOOt"),
    array('test12', 0, "-c -r=root -s"),
    /* 17 */ array('test13', 0, "-r=root -s --start=\"2\" --index-items -l -a"),
    array('test16', 0, "-h=xyz -i"),
    array('01', 0, "", TRUE, TRUE),
    array('01', 0, "--help", TRUE, TRUE, TRUE)
);

function getCommand($commands)
{
    $errCommand = array(
        300 => array('01', 1, "--array-size -a"),
        array('01', 1, "--index-items -t"),
        array('01', 1, "--start=5"),
        array('01', 1, "--help"),
        array('01', 1, "--help -a"),
        array('02', 50, "-r=\"&&\""),
        array('01', 50, "--item-name=\"<<\""),
        array('02', 50, "--array-name=\"$\""),
        array('01', 1, "-t --start=asd"),
        array('01', 1, "-t --start=-5"),
        array('01', 50, "--item-name=\"<item>\""),
        array('01', 50, "--array-name=\"<array>\""),
        array('12', 51, ""),
        array('12', 51, "-c -h=\"<\""),
        array('01', 1, "-t --start=-5"),
        array('01', 1, "-r root"),
        array('01', 1, "-rroot"),
        array('07', 4, ""),
        array('08', 4, ""),
        array('09', 4, ""),
        array('10', 4, ""),
        array('01', 1, "-h="),
        array('13', 51, "-h=\"!\""),
        array('13', 51, "-h=\"?\""),
        array('test14', 51, "-r=root"),
        array('test15', 50, "--array-name=\"b<a>d\""),
        array('04', 50, "--array-name=\"b<a>d\"")
    );
    return ($commands
            + getSubArray(30, '01')
            + getSubArray(60, '02')
            + getSubArray(100, '03')
            + getSubArray(140, '04')
            + getSubArray(180, '05')
            + getSubArray(220, '06')
            + getSubArray(260, '11')
            + $errCommand);
}

//$startId+26 => array($source, 0, "-h = \"&&\"")
//$startId + 25 => array($source, 0, "-h=_")
function getSubArray($startId, $source)
{

    $data = array(
        $startId => array($source, 0, ""),
        $startId + 1 => array($source, 0, "-h=asd"),
        $startId + 2 => array($source, 0, "-n"),
        $startId + 3 => array($source, 0, "-r=root_elem"),
        $startId + 4 => array($source, 0, "--array-name=pole"),
        $startId + 5 => array($source, 0, "--item-name=polozka"),
        $startId + 6 => array($source, 0, "-s"),
        $startId + 7 => array($source, 0, "-i"),
        $startId + 8 => array($source, 0, "-l"),
        $startId + 9 => array($source, 0, "-c"),
        $startId + 10 => array($source, 0, "-a"),
        $startId + 11 => array($source, 0, "--array-size"),
        $startId + 12 => array($source, 0, "-t"),
        $startId + 13 => array($source, 0, "--index-items"),
        $startId + 14 => array($source, 0, "-t --start=5"),
        $startId + 15 => array($source, 0, "-t -a"),
        $startId + 16 => array($source, 0, "-t --array-size --start=6"),
        $startId + 17 => array($source, 0, "-c -s"),
        $startId + 18 => array($source, 0, " -s -l"),
        $startId + 19 => array($source, 0, "-c -s -i"),
        $startId + 20 => array($source, 0, " -s -i -l"),
        $startId + 21 => array($source, 0, "-c -l"),
        $startId + 22 => array($source, 0, "-c -i -l"),
        $startId + 23 => array($source, 0, "-c -i"),
    );
    return ($data);
}

//$startId+26 => array($source, 0, "-h=\"&&\"")
//$startId + 25 => array($source, 0, "-h=_")


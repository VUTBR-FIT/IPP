<?php

#JSN:xcerny63

final class Error
{

    const
            CREATING_CLASS_FAILED = -1,
            ALL_OK = 0,
            BAD_PARAMS = 1,
            INPUTFILE_NOT_EXIST = 2,
            OUTPUTFILE_ERR = 3,
            INPUT_BAD_STRUCTURE = 4,
            BAD_ROOT_NAME = 50,
            BAD_INPUT_DATA = 50,
            BAD_ELEMENT_NAME = 51;

}

final class Params
{

    const
            HELP = '--help',
            INPUT = '--input',
            OUTPUT = '--output',
            H = '-h',
            N = '-n',
            R = '-r',
            ARRAY_NAME = '--array-name',
            ITEM_NAME = '--item-name',
            S = '-s',
            I = '-i',
            L = '-l',
            C = '-c',
            A = '-a',
            ARRAY_SIZE = '--array-size',
            T = '-t',
            INDEX_ITEMS = '--index-items',
            START = '--start';

    /** @var array Prevodni tabulka */
    public static $tableParams = array(
        Params::HELP => 'help',
        Params::INPUT => 'input',
        Params::OUTPUT => 'output',
        Params::H => 'h',
        Params::N => 'n',
        Params::R => 'r',
        Params::ARRAY_NAME => 'array-name',
        Params::ITEM_NAME => 'item-name',
        Params::S => 's',
        Params::I => 'i',
        Params::L => 'l',
        Params::C => 'c',
        Params::A => 'a',
        Params::ARRAY_SIZE => 'array-size',
        Params::T => 't',
        Params::INDEX_ITEMS => 'index-items',
        Params::START => 'start'
    );

    /*
     * --help
     * --input=...
     * --output=...
     * -h=...
     * -n
     * -r=...
     * --array-name=...
     * --item-name=...
     * -s
     * -i
     * -l
     * -c
     * -a
     * --array-size
     * -t
     * --index-items
     * --start=...
     */

    /** @var string "h::nr:silcat" */
    //public static $shortParams = ;

    /**
     * Vraci longParams pro funkci getopt();
     *
     * @return array
     */
    public static function getLongParams()
    {
        return (array(
            self::$tableParams[Params::HELP],
            self::$tableParams[Params::INPUT] . ':',
            self::$tableParams[Params::OUTPUT] . ':',
            self::$tableParams[Params::ARRAY_NAME] . ':',
            self::$tableParams[Params::ITEM_NAME] . ':',
            self::$tableParams[Params::ARRAY_SIZE],
            self::$tableParams[Params::INDEX_ITEMS],
            self::$tableParams[Params::START] . ':'
        ));
    }

    /**
     * Vraci shortParams pro funkci getopt();
     *
     * @return string
     */
    public static function getShortParams()
    {
        return (self::$tableParams[Params::H] . ':' .
                self::$tableParams[Params::N] .
                self::$tableParams[Params::R] . ':' .
                self::$tableParams[Params::S] .
                self::$tableParams[Params::I] .
                self::$tableParams[Params::L] .
                self::$tableParams[Params::C] .
                self::$tableParams[Params::A] .
                self::$tableParams[Params::T]);
    }

    public static function getHelp()
    {
        $help = "\nIPP Projekt - JSON2XML\n";
        $help .= "2015/2016\n";
        $help .= "Vstupní parametry:\n";
        $help .= "\t--help\t\t\t - napise napovedu\n";
        $help .= "\t--input=filename\t - zadany vstupni JSON soubor (kodovani UTF-8)\n";
        $help .= "\t--output=filename\t - zadany vystupni XML soubor (kodovani UTF-8)\n";
        $help .="\t-h=subst\t\t - nahrazuje nepovoleny znak v XML tagu retezcem 'subst' (defaultne '-')\n";
        $help .= "\t-n\t\t\t - negenerovat XML hlavicku\n";
        $help .= "\t-r=root-element\t\t - jmeno korenoveho tagu, pokud neni zadan, tak korenovy tag neexituje\n";
        $help .= "\t--array-name=array-element\t - umoznuje prejmenovat element obsahujici pole (defaultne 'array')\n";
        $help .= "\t--item-name=item-element\t - umoznuje prejmenovat jmeno elementu obshaujici prvek pole (defaultne 'item')\n";
        $help .= "\t-s\t\t\t - hodnoty (v dvojcich i v polich) typu string budou trasformovany na textove elementy misto atributu\n";
        $help .= "\t-i\t\t\t - hodnoty (v dvojcich i v polich) typu number budou trasformovany na textove elementy misto atributu\n";
        $help .= "\t-l\t\t\t - hodnoty literaru (true, false, null) budou transformovany na elementy <true/>...\n";
        $help .= "\t-c\t\t\t - aktivaceprekladu problematickych znaku na odpovidajici zapisy v XML\n";
        $help .= "\t-a, --array-size\t - u pole bude doplnena hodnota o atribut size s uvedenym poctem prvku\n";
        $help .= "\t-t, --index-items\t - u kazdeho prvku pole bude uvedeny atribut index s indexem prvku v poli\n"; //od 1
        $help .= "\t--start=n\t\t - inicializace citace pro undexaci prvku pole\n"; //musi byt take --index-items

        return ($help);
    }

}

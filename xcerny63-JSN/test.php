<?php

define('SCRIPT', './jsn.php');
define('TEST_DIR', __DIR__ . '/tests/');

$inputParams = getopt('', array('all'));

function diffArray($arr1, $arr2)
{
    foreach ($arr2 as $key => $value) {
        if (array_key_exists($key, $arr1) || ((int) $key == 0 && array_key_exists(0, $arr1))) {
            if (is_array($value)) {
                if (diffArray($arr1[$key], $value) === FALSE) {
                    return (FALSE);
                }
            } else {

                if ($value != $arr1[$key]) {
                    return(FALSE);
                }
            }
        } else {
            return (FALSE);
        }
    }
    return (TRUE);
}

if (!file_exists(__DIR__ . '/' . SCRIPT)) {
    echo "File jsn.php not exists.";
    exit(1);
}
error_reporting(0);
require_once TEST_DIR . 'commands.php';
$arrComand = getCommand($commands);
$total = count($arrComand);

$ok = 0;
$bad = 0;

echo "\n------ START TESTS --------\n";

$time_start = microtime(TRUE);
if (file_exists("logs.log")) exec("rm logs.log");

foreach ($arrComand as $key => $options) {

    list($name, $result, $opt) = $options;

    if (file_exists(TEST_DIR . "source/$name.json")) {
        $input = TEST_DIR . "source/$name.json";
    } else {
        $input = TEST_DIR . "source/$name.jsn";
    }

    $output = TEST_DIR . "output/$key.xml";
    $path = "";
    if (count($options) == 3) {
        $path .= " --input=$input --output=$output ";
    } elseif (count($options) == 4) {
        if ($options[3] === TRUE) {
            $path .= " --output=$output < $input ";
        } else {
            $path .= " --output=$output --input=$input ";
        }
    } elseif (count($options) == 5) {
        if ($options[4] === TRUE) {
            if ($options[3] === TRUE) {
                $path .= " < $input > $output ";
            } else {
                $path .= " --input=$input > $output ";
            }
        } else {
            if ($options[3] === TRUE) {
                $path .= " --output=$output < $input ";
            } else {
                $path .= " --output=$output --input=$input ";
            }
        }
    } elseif (count($options) == 6) {

    }
    $cmd = "php -d open_basedir=\"\" " . SCRIPT . " $opt $path";
    exec("echo TEST$key >> logs.log");
    exec("echo $cmd >> logs.log");
    $error = exec($cmd . ' 2>> logs.log', $exec, $outputCode);

    if ($outputCode != $result) {
        echo "[ERROR] \tTEST $key FAILED - $outputCode (expcted: $result)\n";

        $bad++;
        continue;
    } elseif ($outputCode == $result && (($result != 0)) || (count($options) == 6)) {
        echo "[OK] \t\tTEST $key CODE OK\n";
        $ok++;
        continue;
    }
    if (!file_exists(TEST_DIR . 'result/' . $key . '.xml')) {
        $bad++;
        echo "[ERROR] TEST $key - XML WITH RESULTS - MISSED\n";
        continue;
    }

    $contentResult = file_get_contents(TEST_DIR . 'result/' . $key . '.xml');
    $contentOutput = file_get_contents(TEST_DIR . 'output/' . $key . '.xml');

    $objResult = new DOMDocument();
    $objOutput = new DOMDocument();

    $header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    if (strpos($contentOutput, $header) !== strpos($contentResult, $header)) {
        $bad++;
        echo "[ERROR] \tTEST $key FAILED HEADER\n";
        continue;
    }
    $objResult->loadXML("<__ROOT__>" . str_replace(array($header, "\n"), "", $contentResult) . "</__ROOT__>");
    $objOutput->loadXML("<__ROOT__>" . str_replace(array($header, "\n"), "", $contentOutput) . "</__ROOT__>");

    $arrResult = json_decode(json_encode((array) simplexml_import_dom($objResult)), TRUE);
    $arrOutput = json_decode(json_encode((array) simplexml_import_dom($objOutput)), TRUE);


    if (diffArray($arrOutput, $arrResult)) {
        echo "[OK] \t\tTEST $key OK ALL\n";
        if (!array_key_exists('all', $inputParams)) exec("rm $output");
        $ok++;
    } else {
        $bad++;
        echo "[ERROR] \tTEST $key FAILED\n";
    }
}
$time = microtime(TRUE) - $time_start;
echo "\n\n TOTAL: \tOK($ok) | ERROR($bad)\t\t TOTAL($total)\n >>>>>> $time seconds\n";

#! /usr/bin/env python3

#MKA:xcerny63
'''
Created on Mar 28, 2016

@author: xcerny63
'''

import sys
from funkce import *

if __name__ == '__main__':
    '''paramInput = False
    inputName = ""
    paramOutput = False
    outputName = ""
    paramF = False
    paramM = False
    paramI = False
    error = False'''

    if len(sys.argv) == 2 :
        if sys.argv[1] == "--help":
            printHelp()
            sys.exit(0)
    
    (error, paramInput, inputName, paramOutput, outputName, paramM, paramF, paramI) = validateInput(len(sys.argv), sys.argv)
    if error != 0 :
        printError("Chyba pri kontrole vstupnich parametru")
        sys.exit(error)
    
    (error, contents) = getFileContents(inputName)
    if error != 0 :
        printError("Chyba pri nacitani dat")
        sys.exit(error)
    if paramI : contents = contents.lower()
    (error, KAArrState, KAArrInput, KAArrRule, KAStrStart, KAArrEnd) = validateKA(contents)
    if error != 0 :
        printError("Chyba pri zpracovani vstupnich dat")
        sys.exit(error)
    if paramF : 
        result = findTraps(KAArrInput, KAArrRule, KAArrEnd)
        if len(result) == 0 :
            result = "0"
    else :
        if paramM :
            (KAArrState, KAArrInput, KAArrRule, KAStrStart, KAArrEnd) = minimize(KAArrState, KAArrInput, KAArrRule, KAStrStart, KAArrEnd)
    
        result = printMKA(KAArrState, KAArrInput, KAArrRule, KAStrStart, KAArrEnd)
    
    error = saveOutput(outputName, result)
    sys.exit(error)
    
    print (result)